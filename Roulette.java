import java.util.Scanner;
public class Roulette
{
    public static Scanner in = new Scanner(System.in);
    public static int betNum;
    public static int betMoney;
    public static int balance = 100;  

    public static void main(String[] args)
    {
        RouletteWheel wheel = new RouletteWheel();
        if (bet() == false)
        {
            return;
        }
        else
        {
            if (spin(wheel) == true)
            {
                balanceAdd(betMoney*2);
            }
            else
            {
                balanceRem();
            }
        }
    }

    public static boolean bet()
    {
        System.out.println("Would you like to bet?");
        String answer = in.nextLine();
        if (answer.equals("no"))
        {
            return false;
        }
        else
        {
            boolean check = false;
            while (check == false)
            {
                System.out.println("What number do you want ot bet on? (1-37)");
                betNum = in.nextInt();
                if (betNum >=0 && betNum < 38)
                {
                    check = true;
                }
            }
            check = false;
            while (check == false)
            {
                System.out.println("How much do you want to bet?");
                betMoney = in.nextInt();
                if (betMoney <= balance)
                {
                    check = true;
                }
            }
            return true;        
        }
    }

    public static boolean spin(RouletteWheel w)
    {
        w.spin();
        if(w.getValue() == betNum)
        {
            System.out.println("You won!");
            return true;
        }
        else{
            return false;
        }
    }

    public static void balanceAdd(int m)
    {
        balance += m;
    }

    public static void balanceRem()
    {
        balance -= betMoney;
    }
}
